#pragma once

#include "Option.hpp"
#include "BlackScholesModel.hpp"
#include "pnl/pnl_random.h"
#include "pnl/pnl_random.h"
#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"
#include "pnl/pnl_array.h"
#include "pnl/pnl_basis.h"

/**
 * \class Classe de Monte Carlo.
 * \brief Classe utilisant un modèle de BlackScholes à buts de pricing d'options américaines à l'aide de la méthode de Longstaff-Schwartz.
 * \details Cette classe permet d'obtenir le prix en t0 d'une option américaine selon sa maturité, son spot, la volatilité, le taux sans fixe...
 */

class MonteCarlo
{
public:
    /**
    * \brief Modèle de Black Scholes utilisé.
    *
    */
    BlackScholesModel *mod_; /*! pointeur vers le modèle */
    /**
    * \brief Pointeur sur l'option.
    *
    */
    Option *opt_; /*! pointeur sur l'option */
    /**
    * \brief random number generator.
    *
    */
    PnlRng *rng_; /*! pointeur vers le rng */
    /**
    * \brief matrice contenant une trajectoire du modèle.
    *
    */
    PnlMat *path_;
    /**
    * \brief Constructeur de la classe.
    *
    */
    MonteCarlo(BlackScholesModel *mod, Option *opt, PnlRng *rng);
    /**
    * \brief Destructeur de la classe.
    *
    */
    ~MonteCarlo();

    /**
     *  \brief Calcule le prix de l'option à la date 0
     *
     * @return valeur de l'estimateur Monte Carlo
     */
    double price(long M, int nbTimeSteps, int degree);

    /**
     * \brief Met à jour la stratégie
     *
     * @return nouveau temps d'arret optimal
     */
    void updatePolicy(PnlBasis *basis, PnlVect *psi, PnlVect *S_ti_m, PnlVectInt *tau, int i, int m);



};


