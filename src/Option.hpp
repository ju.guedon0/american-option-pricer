#pragma once

#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"
#include <math.h> 

/**
 * \class Option.
 * \brief Classe abstraite représentant une option.
 * \details Cette classe abstraite a pour but d'être héritée par différentes options.
 */
class Option
{
public:
    /**
    * \brief Maturité de l'option.
    *
    */
    double T_; /// maturité
    /**
    * \brief Nombre de dates d'exercice de l'option.
    *
    */
    int dates_; /// nombre de dates d'exercice
    /**
    * \brief Dimension du modèle.
    *
    */
    int size_; /// dimension du modèle, redondant avec BlackScholesModel::size_
    /**
    * \brief Strike de l'option.
    *
    */
    double K_; ///strike de l'option
    /**
    * \brief Coefficients du payoff.
    *
    */
    PnlVect *lambda_; /// payoff coefficients
    /**
    * \brief Constructeur de la classe.
    *
    */
    Option(double T, int dates, int size, double K, PnlVect *lambda);
    /**
    * \brief Destructeur de la classe.
    *
    */
    ~Option();
    
    /**
     * Calcule la valeur du payoff sur la trajectoire
     *
     * @param[in] path est une matrice de taille (dates_+1) x size_
     * contenant une trajectoire du modèle telle que créée
     * par la fonction asset.
     * @return phi(trajectoire)
     */


    virtual double payoff(const PnlVect *path) = 0;
};


