#include "Option.hpp"

Option::Option(double T, int dates, int size, double K, PnlVect *lambda)
{
    this->T_ = T;
    this->dates_ = dates;
    this->size_ = size;
    this->K_ = K;
    this->lambda_ = lambda;
}

Option::~Option(){
    pnl_vect_free(&lambda_);
}
