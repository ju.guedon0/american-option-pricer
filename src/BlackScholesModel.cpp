#include <iostream>
#include "BlackScholesModel.hpp"

using namespace std;

BlackScholesModel::BlackScholesModel(int size, double r, double rho, PnlVect *sigma, PnlVect *divid, PnlVect *spot){
    this->size_ = size;
    this->r_ = r;
    this->rho_ = rho;
    this->sigma_ = sigma;
    this->divid_ = divid;
    this->spot_ = spot;
        
    /// Creation de la matrice de Cholesky
    PnlMat *gamma = pnl_mat_create_from_double(size, size, rho_);
    for(int i=0; i<size_; i++){
        pnl_mat_set(gamma, i, i, 1);
    };
    pnl_mat_chol(gamma);
    this->L_ = gamma;

    /// objets intermediaires de calcul
    this->Gi_ = pnl_vect_create_from_zero(size);
    this->LGi_ = pnl_vect_create_from_zero(size);
    this->S_i_ = pnl_vect_create_from_zero(size);
}

BlackScholesModel::~BlackScholesModel(){
    pnl_vect_free(&Gi_);
    pnl_vect_free(&LGi_);
    pnl_vect_free(&S_i_);
    pnl_vect_free(&sigma_);
    pnl_vect_free(&divid_);
    pnl_vect_free(&spot_);
    pnl_mat_free(&L_);

}

void BlackScholesModel::asset(PnlMat *path, double T, int nbTimeSteps, PnlRng *rng){
    double stepsize = T/nbTimeSteps;
    double sqrt_stepsize = sqrt(stepsize);
    pnl_mat_set_row(path, this->spot_, 0);
    for (int ti = 1; ti < nbTimeSteps + 1; ti++)
    {
        pnl_vect_rng_normal_d(Gi_, this->size_, rng);
        pnl_mat_mult_vect_inplace(LGi_, L_, Gi_);
        pnl_mat_get_row(S_i_, path, ti-1);
        for (int d = 0; d < this->size_; d++)
        {
            /// on recupere S_(i-1, d), sigma_d et Ld*Gi
            double Si_d = GET(S_i_, d);
            double sigma_d = GET(sigma_, d);
            double Ld_Gi = GET(LGi_, d);
            /// calcul de Sti+1,d et stockage
            double Sti_d = Si_d * exp((this->r_- GET(divid_, d) - ((sigma_d*sigma_d)/2))*stepsize + sigma_d*sqrt_stepsize*Ld_Gi);
            pnl_mat_set(path, ti, d, Sti_d);
        }
    }
}



