#pragma once
#include "Option.hpp"

/**
 * \class Basket.
 * \brief Classe representant un basket.
 * \details Cette classe decrit les caracteristiques d'un basket ainsi que la maniere dont son payoff est calcule.
 */

class Basket: public Option
{
    public:

        /**
         * \brief Constructeur de la classe.
         */
        Basket(double T, int dates, int size, double K, PnlVect *lambda);

        /**
         * Calcule la valeur du payoff du basket sur la trajectoire donnee.
         *
         * @param[in] path est une matrice de taille (N+1) x d
         * contenant une trajectoire du mod�le telle que cree
         * par la fonction asset.
         * @return phi(trajectoire)
         */
        double payoff(const PnlVect *path);
};