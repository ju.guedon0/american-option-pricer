#pragma once
#include <string>

/**
 * \class PricingResults.
 * \brief Classe à buts d'affichage.
 * \details Cette classe permet d'afficher les resultats obtenus en pricant des options.
 */

class PricingResults
{
private:
    /**
    * \brief Prix a afficher.
    *
    */
    double price;
public:
    /**
     * Constructeur de la classe
     */
    PricingResults(double price)
        : price(price)
    { }
    /**
     * Destructeur de la classe
     */
    ~PricingResults() { }

    friend std::ostream& operator<<(std::ostream &stm, const PricingResults &res);
};
