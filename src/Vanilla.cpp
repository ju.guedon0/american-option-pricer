#include "Vanilla.hpp"
#include "pnl/pnl_matrix.h"
#include <iostream>
using namespace std;

Vanilla::Vanilla(double T, int dates, int size, double K, PnlVect *lambda):Option(T, dates, size, K, lambda){
    
}; 

double Vanilla::payoff(const PnlVect *path){
    return max((double)pnl_vect_get(this->lambda_, 0)*pnl_vect_get(path, 0) - this->K_, 0.0);
}
