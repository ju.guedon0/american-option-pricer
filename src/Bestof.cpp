#include "Bestof.hpp"
#include "pnl/pnl_matrix.h"
#include <iostream>
using namespace std;

Bestof::Bestof(double T, int dates, int size, double K, PnlVect *lambda):Option(T, dates, size, K, lambda){
    
}; 

double Bestof::payoff(const PnlVect *path){
    double res = -INFINITY;
    for (int i = 0; i < this->size_; i++)
    {
        res = max(pnl_vect_get(this->lambda_, i)*pnl_vect_get(path, i), res);
    }
    res = max(res - this->K_, 0.0);
    return res;
}
