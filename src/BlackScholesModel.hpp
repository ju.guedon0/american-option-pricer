#pragma once

#include "pnl/pnl_random.h"
#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"

/**
 * \class Modèle de BlackScholes.
 * \brief Classe représentant un modèle de BlackScholes utilisé par une méthode de MonteCarlo.
 * \details Cette classe décrit les caractéristiques d'un modèle de BlackScholes, ainsi que ses différentes méthodes.
 */

class BlackScholesModel
{
public:
    /**
    * \brief Nombre d'actifs du modèle.
    *
    */
    int size_; /// nombre d'actifs du modèle
    /**
    * \brief Taux d'intérêt.
    *
    */
    double r_; /// taux d'intérêt
    /**
    * \brief Paramètre de corrélation.
    *
    */
    double rho_; /// paramètre de corrélation
    /**
    * \brief Vecteur des volatilités.
    *
    */
    PnlVect *sigma_; /// vecteur de volatilités
    /**
    * \brief Vecteur des dividendes.
    *
    */
    PnlVect *divid_; /// vecteur des dividendes
    /**
    * \brief Valeur initiale des sous-jacents.
    *
    */
    PnlVect *spot_; /// valeurs initiales des sous-jacents
    /**
    * \brief Décomposition de Cholesky de la matrice de corrélaton.
    *
    */
    PnlMat *L_; /// decomposition de cholesky de la matrice de correlation
    /**
    * \brief Vecteur temp servant à stocker des lignes de path pour faire certains calculs.
    *
    */
    PnlVect *S_i_; 
    /**
    * \brief Vecteur temp de nombres générés suivant une loi normale servant à faire des calculs.
    *
    */
    PnlVect *Gi_;
    /**
    * \brief Vecteur servant à faire le produit de lignes des matrices Cholesky/Gauss.
    *
    */
    PnlVect *LGi_;
    /**
    * \brief Vecteur temporaire.
    *
    */
    PnlVect *tmp_;

    /**
    * \brief Constructeur de la classe.
    *
    */
    BlackScholesModel(int size, double r, double rho, PnlVect *sigma, PnlVect *divid, PnlVect *spot);

    /**
    * \brief Destructeur de la classe.
    *
    */
    ~BlackScholesModel();
    /**
     * Génère une trajectoire du modèle et la stocke dans path
     *
     * @param[out] path contient une trajectoire du modèle.
     * C'est une matrice de taille (nbTimeSteps+1) x d
     * @param[in] T  maturité
     * @param[in] nbTimeSteps nombre de dates de constatation
     */
    void asset(PnlMat *path, double T, int nbTimeSteps, PnlRng *rng);


};
