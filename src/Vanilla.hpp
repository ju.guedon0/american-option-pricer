#pragma once
#include "Option.hpp"

/**
 * \class Vanilla.
 * \brief Classe representant une option vanille.
 * \details Cette classe herite de option et decrit une option simple: l'option vanille.
 */

class Vanilla: public Option
{
    public:
        /**
        * \brief Constructeur de la classe.
        *
        */
        Vanilla(double T, int dates, int size, double K, PnlVect *lambda);
        /**
         * Calcule la valeur du payoff du basket sur la trajectoire donn�e.
         *
         * @param[in] path est une matrice de taille (N+1) x d
         * contenant une trajectoire du mod�le telle que cr��e
         * par la fonction asset.
         * @return phi(trajectoire)
         */
        double payoff(const PnlVect *path);
};