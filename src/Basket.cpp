#include "Basket.hpp"
#include "pnl/pnl_matrix.h"
#include <iostream>
using namespace std;

Basket::Basket(double T, int dates, int size, double K, PnlVect *lambda):Option(T, dates, size, K, lambda){
    
}; 

double Basket::payoff(const PnlVect *path){
    double res = 0;
    for (int i = 0; i < this->size_; i++)
    {
        res += pnl_vect_get(this->lambda_, i)*pnl_vect_get(path, i);
    }
    res = max(res - this->K_, 0.0);
    return res;
}
