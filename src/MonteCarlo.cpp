#include <iostream>
#include "MonteCarlo.hpp"
#include "Option.hpp"
#include "pnl/pnl_basis.h"

using namespace std;

MonteCarlo::MonteCarlo(BlackScholesModel *mod, Option *opt, PnlRng *rng){
    this->mod_ = mod;
    this->opt_ = opt;
    this->rng_ = rng;
    this->path_ = pnl_mat_create_from_zero(opt->dates_+1, opt->size_);
}

MonteCarlo::~MonteCarlo(){
    delete mod_;
    delete opt_;
    pnl_mat_free(&path_);
};

double MonteCarlo::price(long M, int nbTimeSteps, int degree){

    // Initialisation des M tirages--------------------------------------------------
    PnlArray *S = pnl_array_create(M);
    for (int m = 0; m < M; m++)
    {
        mod_->asset(this->path_, this->opt_->T_, nbTimeSteps, this->rng_);
        //On peut éviter les copies en utilisant des pont brownien , cf fav google
        PnlMat *path_cp = pnl_mat_copy(path_);
        pnl_array_set(S, m, (PnlObject*)path_cp);
    }
    // Pricing----------------------------------------------------------------------
    // On pose une base de L2 
    int dim = this->mod_->size_;
    PnlBasis *basis =  pnl_basis_create_from_degree(PNL_BASIS_HERMITE , degree, dim);
    //Algo LS, initialisation des variables
    double delta = this->opt_->T_/nbTimeSteps;
    PnlVectInt *tau = pnl_vect_int_create_from_int(M, (int)(this->opt_->T_/delta));
    PnlVect *psi = pnl_vect_create_from_zero(dim);
    PnlVect *S_ti_m = pnl_vect_create_from_zero(dim);
    PnlVect *tmp = pnl_vect_create_from_zero(dim);
    PnlMat *x = pnl_mat_create_from_zero(M, dim);
    PnlVect *y = pnl_vect_create_from_zero(M);
    double tmp_exp_payoff;
    int tau_m;
    for (int i = nbTimeSteps - 1; i > 0; i--)
    {      
        for (long m = 0; m < M; m++)
        {
            // on calcule x
            pnl_mat_get_row(tmp, (PnlMat*)pnl_array_get(S, m), i);          
            pnl_mat_set_row(x, tmp, m);
            // On calcule y
            tau_m = GET_INT(tau, m);
            pnl_mat_get_row(tmp, (PnlMat*)pnl_array_get(S, m), tau_m);
            tmp_exp_payoff = this->opt_->payoff(tmp)*exp(-this->mod_->r_*delta*(tau_m - i));
            pnl_vect_set(y, m, tmp_exp_payoff);
        }

        //calcul de psi
        pnl_basis_fit_ls(basis, psi, x, y);
        //mise à jour de tau
        for (long m = 0; m < M; m++)
        {
            pnl_mat_get_row(S_ti_m, (PnlMat*)pnl_array_get(S, m), i);
            this->updatePolicy(basis, psi, S_ti_m, tau, i, m);
        }
    }
    //Calcul de P0
    double res = 0;
    for (long m = 0; m < M; m++)
    {
        tau_m = GET_INT(tau, m);
        pnl_mat_get_row(tmp, (PnlMat*)pnl_array_get(S, m), tau_m);
        res += this->opt_->payoff(tmp)*exp(-this->mod_->r_*tau_m*delta); //on a posé t0 = 0
    }
    res = max(res/M, this->opt_->payoff(this->mod_->spot_));

    pnl_basis_free(&basis);
    pnl_vect_int_free(&tau);
    pnl_vect_free(&psi);
    pnl_vect_free(&S_ti_m);
    pnl_vect_free(&tmp);
    pnl_vect_free(&y);
    pnl_mat_free(&x);
    pnl_array_free(&S);
    return res;
}



void MonteCarlo::updatePolicy(PnlBasis *basis, PnlVect *psi, PnlVect *S_ti_m, PnlVectInt *tau, int i, int m){
    if (this->opt_->payoff(S_ti_m) >= pnl_basis_eval_vect(basis, psi, S_ti_m))
    {
        pnl_vect_int_set(tau, m, i);
    } 
}
