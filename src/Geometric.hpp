#pragma once
#include "Option.hpp"

/**
 * \class Geometric.
 * \brief Classe representant un put geometrique.
 * \details Cette classe decrit les caracteristiques d'une option geometrique ainsi que la maniere dont son payoff est calcule.
 */

class Geometric: public Option
{
    public:
        /**
         * \brief Constructeur de la classe.
         */
        Geometric(double T, int dates, int size, double K, PnlVect *lambda);
        /**
         * Fonction abstraite calculant la valeur du payoff de l'option sur la trajectoire donnee.
         *
         * @param[in] path est une matrice de taille (N+1) x d
         * contenant une trajectoire du modele telle que cree
         * par la fonction asset.
         * @return phi(trajectoire)
         */
        double payoff(const PnlVect *path);
};