#include <iostream>
#include <ctime>
#include "pnl/pnl_random.h"
#include "pnl/pnl_vector.h"
#include "PricingResults.hpp"
#include "BlackScholesModel.hpp"
#include "Basket.hpp"
#include "Geometric.hpp"
#include "Bestof.hpp"
#include "Vanilla.hpp"
#include "MonteCarlo.hpp"
#include <string>
#include "jlparser/parser.hpp"

using namespace std;

int main(int argc, char **argv)
{
    double T, r, strike, rho;
    PnlVect *spot, *sigma, *divid, *payoff_coef;
    string type;
    int size, dates, degree;
    int n_samples;
    Option *opt;
    PnlRng *rng = pnl_rng_create(PNL_RNG_MERSENNE);
    pnl_rng_sseed(rng, time(NULL));

    char *infile = argv[1];
    Param *P = new Parser(infile);

    P->extract("option type", type);
    P->extract("maturity", T);
    P->extract("dates", dates);
    P->extract("model size", size);
    P->extract("correlation", rho);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    if (P->extract("payoff coefficients", payoff_coef, size, true) == false)
    {
        payoff_coef = pnl_vect_create_from_zero(size);
    }
    P->extract("interest rate", r);
    P->extract("degree for polynomial regression", degree);
    if (P->extract("dividend rate", divid, size, true) == false)
    {
        divid = pnl_vect_create_from_zero(size);
    }
    P->extract("strike", strike);
    P->extract("MC iterations", n_samples);

    if (type == "basket")
    {
        opt = new Basket(T, dates, size, strike, payoff_coef);
    } 
    else if (type == "geometric_put")
    {
        opt = new Geometric(T, dates, size, strike, payoff_coef);
    }
    else if (type == "bestof")
    {
        opt = new Bestof(T, dates, size, strike, payoff_coef);
    }
    else if (type == "vanilla")
    {
        opt = new Vanilla(T, dates, size, strike, payoff_coef);
    }
    
    BlackScholesModel *bs = new BlackScholesModel(size, r, rho, sigma, divid, spot);
    MonteCarlo *mt = new MonteCarlo(bs, opt, rng);
    PricingResults price = PricingResults(mt->price(n_samples, opt->dates_, degree));
    
    cout << price << endl;

    delete mt;
    delete P;
    pnl_rng_free(&rng);
    return 0;
}
