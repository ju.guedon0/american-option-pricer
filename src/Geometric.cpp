#include "Geometric.hpp"
#include "pnl/pnl_matrix.h"
#include <iostream>
using namespace std;

Geometric::Geometric(double T, int dates, int size, double K, PnlVect *lambda):Option(T, dates, size, K, lambda){
    
}; 

double Geometric::payoff(const PnlVect *path){
    double res = 1;
    for (int i = 0; i < this->size_; i++)
    {
        res = res * pnl_vect_get(path, i);
    }
    res = max(this->K_ - pow(res, 1.0/this->size_), 0.0);
    return res;
    
}
