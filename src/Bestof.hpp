#pragma once
#include "Option.hpp"

/**
 * \class Bestof.
 * \brief Classe representant une option performance sur panier.
 * \details Cette classe decrit les caracteristiques d'une option performance ainsi que la maniere dont son payoff est calcule.
 */

class Bestof: public Option
{
    public:
        /**
         * \brief Constructeur de la classe.
         *
         */
        Bestof(double T, int dates, int size, double K, PnlVect *lambda);

        /**
         * Calcule la valeur du payoff du Bestof sur la trajectoire donnee.
         *
         * @param[in] path est une matrice de taille (N+1) x d
         * contenant une trajectoire du modele telle que cree
         * par la fonction asset.
         * @return phi(trajectoire)
         */
        double payoff(const PnlVect *path);
};